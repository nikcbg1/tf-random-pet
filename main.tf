resource "random_pet" "example" {
  length    = "8"
}

output "name" {
  value = "${random_pet.example.id}"
}